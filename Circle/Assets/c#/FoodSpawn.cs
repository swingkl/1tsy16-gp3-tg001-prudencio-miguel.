﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class FoodSpawn : NetworkBehaviour
{
	public GameObject Food;
	public GameObject World;

	// Use this for initialization
	void Start ()
	{
		//transform.position = new Vector3(Mathf.Clamp(Time.time, 1.0F, 25.0F), 0, 0);
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	void Awake()
	{
		InvokeRepeating("Spawn",0,1);
	}

	void Spawn()
	{
		if (isServer)
		{
		Vector3 pos2 = new Vector3 (Random.Range (-100, 100), Random.Range (-50, 50));
		World = (GameObject)Instantiate (Food, pos2, Quaternion.identity);
	    NetworkServer.Spawn(World);
		}
	}
}
