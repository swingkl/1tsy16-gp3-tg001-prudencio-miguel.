﻿using UnityEngine;
using System.Collections;

public class Eating : MonoBehaviour
{

	void Start ()
	{

	}
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnTriggerEnter2D (Collider2D Eat)
	{
		if (Eat.gameObject.tag == "Bit") 
		{
			transform.localScale += new Vector3 (.05f, .05f, 0);
			Destroy (Eat.gameObject);
		}
	}
}
