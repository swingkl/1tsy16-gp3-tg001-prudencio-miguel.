﻿using UnityEngine;
using System.Collections;

public class CTRL : MonoBehaviour
{
	
	public float speed = 5.0f;
	private Vector3 target;

	void Start ()
	{
		target = transform.position;
	}
	
	void Update ()
	{
		/////==============/////
		target = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		target.z = transform.position.z;
		transform.position = Vector3.MoveTowards (transform.position, target, speed * Time.deltaTime);
		/////==============/////
	}    
}