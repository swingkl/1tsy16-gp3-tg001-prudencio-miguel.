﻿using UnityEngine;
using System.Collections;

public class Spawning : MonoBehaviour
{

	public float xRange = 1.0f;
	public Transform[] teleport;
	public GameObject[] prefeb;
	
	void Start ()
	{
		int tele_num = Random.Range (0,3);
		int prefeb_num = Random.Range (0,1);
		
		Instantiate (prefeb [prefeb_num], teleport [tele_num].position, Quaternion.identity);
	}
}
